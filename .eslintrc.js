const fs = require('fs');
const path = require('path');

const prettierOptions = JSON.parse(fs.readFileSync(path.resolve(__dirname, '.prettierrc'), 'utf8'));

module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true, // Add node environment for Next.js specifics
    },
    extends: [
        'next/core-web-vitals',
        'prettier',
        'plugin:@typescript-eslint/recommended',
        'plugin:react/recommended',
        'plugin:react-hooks/recommended',
        'plugin:react/jsx-runtime',
        'plugin:jsx-a11y/recommended',
        'plugin:@tanstack/eslint-plugin-query/recommended',
    ],
    plugins: ['react', 'prettier', '@typescript-eslint', 'jsx-a11y', '@tanstack/query'],

    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
        project: './tsconfig.json',
    },

    rules: {
        'react/jsx-filename-extension': 0,
        'react/react-in-jsx-scope': 'off',
        'no-param-reassign': 0,
        'react/prop-types': 1,
        'react/require-default-props': 0,
        'react/no-array-index-key': 0,
        'react/jsx-props-no-spreading': 0,
        'react/forbid-prop-types': 0,
        'import/no-cycle': 0,
        'import/prefer-default-export': 0,
        'import/no-import-module-exports': 0,
        'react/no-unescaped-entities': 1,
        'jsx-a11y/click-events-have-key-events': 'off',
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': ['off'],
        '@typescript-eslint/no-explicit-any': ['off'],
        'react/jsx-key': 1,
        'import/order': [
            'error',
            {
                alphabetize: {
                    order: 'asc',
                    caseInsensitive: true,
                },
                groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index', 'object', 'type'],
            },
        ],
        'no-console': 1,
        'jsx-a11y/anchor-is-valid': 0,
        'prefer-destructuring': 0,
        'no-shadow': 0,
        'no-unreachable': 1,
        'no-restricted-syntax': 0,
        'import/no-named-as-default': 0,
        'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
        // 'unused-imports/no-unused-imports': 1,
        'react/jsx-boolean-value': 2,
        'prettier/prettier': ['error', prettierOptions],
    },
    settings: {
        react: {
            version: 'detect', // Automatically detect the version of React
        },
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
                moduleDirectory: ['node_modules', 'src/'],
            },
        },
    },
};
